PROC 1 LIBRARY MCLASS(X) UID(&SYSUID)                                   00010000
CONTROL NOLIST FLUSH NOMSG                                              00020000
EDIT 'PPST.CNTL(COMPJCL)' CNTL                                          00030000
RENUM                                                                   00040000
VERIFY OFF                                                              00050000
C 1 99999 /UID/&UID./ALL                                                00060000
C 1 99999 /XJOBNAME/&SYSUID.1/ALL                                       00070000
C 1 99999 /XCLASS/&MCLASS./ALL                                          00080000
C 1 99999 /XLIB/&LIBRARY./ALL                                           00090000
CONTROL FLUSH MSG                                                       00100000
LIST                                                                    00110000
SUBMIT                                                                  00120000
END NOSAVE                                                              00130000
